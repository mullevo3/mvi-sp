# OCR model for transcribing hand-written dates
NI-MVI Semestral work

## Key files
- **milestone.pdf** (milestone, submitted on 30.11.2023, description of the current progress)
- **report.pdf** (PDF report)
- Jupyter notebooks

## Assignment
During the forms processing, there are multiple inputs, and one of them is a date, usually in the format `DD.MM.YYYY`. These forms are filled out by various people with different writing styles. Sometimes, there are anomalies such as missing parts or cutbacks. The current processing of the form involves manual transcription into the database.

The aim is to create a custom OCR model that can automatically and accurately transcribe digits in those dates. For this purpose, a custom YOLO model is trained for segmenting each character, followed by a custom CNN for character identification.

## Structure
- `milestone.pdf` (milestone submission)
- `data-dates/` (source files)
- `data-dates-processed/` (processed source files)
- `mnist/` (MNIST dataset)
- `models/` (saved models)
- `yolo6/` (trained YOLOv8 model)
- `jupyter notebooks`
- `report.pdf` (PDF report)

## How to run
For running the scripts just open the jupyter notebooks. They can be run independently in order.